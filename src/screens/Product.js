import React, { Component } from "react";
import {
  Dimensions,
  StyleSheet,
  ScrollView
} from "react-native";
import * as Icon from "@expo/vector-icons";

import { Button, Divider, Input, Block, Text } from "../components";
import { theme, mocks } from "../constants";

const { width, height } = Dimensions.get("window");

class Product extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerRight: (
        <Button onPress={() => {}}>
          <Icon.Entypo name="dots-three-horizontal" color={theme.colors.gray} />
        </Button>
      )
    };
  };

  state = {
    data:[]
  };

  componentDidMount() {
    this.fetchData();
  }

  fetchData = async () => {
    const { navigation } = this.props;
    const id  = navigation.getParam('_id', 'default value')
    const response = await fetch("https://cat-fact.herokuapp.com/facts/"+id);
    const json = await response.json();
    this.setState({data: json});
  }

  render() {
    const data = this.state.data
    return (
      <ScrollView showsVerticalScrollIndicator={false}>

        <Block style={styles.product}>
          <Text h2 bold>
           Fun Facts {data.type}
          </Text>

          <Text gray light height={22}>
            {data.text}
          </Text>

          <Divider margin={[theme.sizes.padding * 0.9, 0]} />
        </Block>
      </ScrollView>
    );
  }
}


export default Product;

const styles = StyleSheet.create({
  product: {
    paddingHorizontal: theme.sizes.base * 2,
    paddingVertical: theme.sizes.padding
  },
  tag: {
    borderColor: theme.colors.gray2,
    borderWidth: StyleSheet.hairlineWidth,
    borderRadius: theme.sizes.base,
    paddingHorizontal: theme.sizes.base,
    paddingVertical: theme.sizes.base / 2.5,
    marginRight: theme.sizes.base * 0.625
  },
  image: {
    width: width / 3.26,
    height: width / 3.26,
    marginRight: theme.sizes.base
  },
  more: {
    width: 55,
    height: 55
  }
});
