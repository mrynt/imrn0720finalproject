import React, { Component } from "react";
import {
  Dimensions,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  FlatList,
  View
} from "react-native";

import {  Button, Block, Text } from "../components";
import { theme } from "../constants";

const { width,height } = Dimensions.get("window");

class Browse extends Component {
  state = {
    active: "Products",
    categories: [],
    data:[]
  };

  componentDidMount() {
    this.setState({ categories: this.props.categories });
    this.fetchData();
  }

  fetchData = async () => {
    const response = await fetch("https://cat-fact.herokuapp.com/facts/random?amount=20");
    const json = await response.json();
    this.setState({data: json});
  }


  renderExplore() {
    const {  navigation } = this.props;

    return (
      <Block style={{ marginBottom: height / 5 }}>
        
          <FlatList
          data={this.state.data}
          keyExtractor={(x,i) => i}
          renderItem={({item}) => 
          <View style={styles.bodySkill}>
            <TouchableOpacity
          style={[styles.image, styles.mainImage]}
          onPress={() => navigation.navigate("Product",{_id:item._id})}
        >
          <Text>{item.text}</Text>
          </TouchableOpacity>
          </View>
          }
          
          />
      </Block>
    );
  }



  render() {
    const {  navigation } = this.props;


    return (
      <Block>
        <Block flex={false} row center space="between" style={styles.header}>
          <Text h1 bold>
            The Animal Fact
          </Text>
          <Button onPress={() => navigation.navigate("Settings")}>
            <Image source={require("../assets/images/avatar.png")} style={styles.avatar} />
          </Button>
        </Block>


        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{ paddingVertical: theme.sizes.base * 2 }}
        >
          {this.renderExplore()}
        </ScrollView>
      </Block>
    );
  }
}




export default Browse;

const styles = StyleSheet.create({
  header: {
    paddingHorizontal: theme.sizes.base * 2
  },
  avatar: {
    height: theme.sizes.base * 2.2,
    width: theme.sizes.base * 2.2
  },
  tabs: {
    borderBottomColor: theme.colors.gray2,
    borderBottomWidth: StyleSheet.hairlineWidth,
    marginVertical: theme.sizes.base,
    marginHorizontal: theme.sizes.base * 2
  },
  tab: {
    marginRight: theme.sizes.base * 2,
    paddingBottom: theme.sizes.base
  },
  active: {
    borderBottomColor: theme.colors.secondary,
    borderBottomWidth: 3
  },
  bodySkill:{
    flexDirection: 'row',
    backgroundColor:"#B4E9FF",
    marginTop:5,
    marginBottom:5,
    borderRadius:5,
    padding:5,
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 6,
    },
    shadowOpacity: 0.39,
    shadowRadius: 4.30,

    elevation: 13,
},
  categories: {
    flexWrap: "wrap",
    paddingHorizontal: theme.sizes.base * 2,
    marginBottom: theme.sizes.base * 3.5
  },
  category: {
    // this should be dynamic based on screen width
    minWidth: (width - theme.sizes.padding * 2.4 - theme.sizes.base) / 2,
    maxWidth: (width - theme.sizes.padding * 2.4 - theme.sizes.base) / 2,
    maxHeight: (width - theme.sizes.padding * 2.4 - theme.sizes.base) / 2
  }
});
